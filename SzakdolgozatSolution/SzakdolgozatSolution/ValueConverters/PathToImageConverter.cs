﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Media.Imaging;

namespace Szakdolgozat
{
    class PathToImageConverter : BaseValueConverter<PathToImageConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
                using (var stream = new FileStream(value.ToString(), FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    return BitmapFrame.Create(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
                }
            else
            {
                return null;
            }
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
