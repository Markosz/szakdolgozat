﻿using System;
using System.Globalization;
using Szakdolgozat.Core;

namespace Szakdolgozat
{
    public class SidebarContentConverter : BaseValueConverter<SidebarContentConverter>
    {
        protected ImageListControl imageListControl = new ImageListControl(IoC.Settings.ImageList);
        protected ImageListControl sharedListControl = new ImageListControl(IoC.Settings.SharedList);

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((SidebarContent)value)
            {
                case SidebarContent.Images:
                    return imageListControl;
                case SidebarContent.Shared:
                    return sharedListControl;
                default:
                    return "Under construction";
            }
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}