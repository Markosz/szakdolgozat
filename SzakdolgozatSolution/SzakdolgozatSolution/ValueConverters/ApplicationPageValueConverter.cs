﻿using System;
using System.Diagnostics;
using System.Globalization;
using Szakdolgozat.Core;

namespace Szakdolgozat
{
    public class ApplicationPageValueConverter : BaseValueConverter<ApplicationPageValueConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((ApplicationPage)value)
            {
                case ApplicationPage.Login:
                    return new LoginPage();

                case ApplicationPage.Main:
                    return new MainPage();

                case ApplicationPage.Register:
                    return new RegisterPage();

                case ApplicationPage.Continue:
                    return new ContinuePage();

                case ApplicationPage.Popout:
                    return new PopoutPage();

                case ApplicationPage.Edit:
                    return new EditPage();

                case ApplicationPage.Share:
                    return new SharePage();

                default:
                    Debugger.Break();
                    return null;
            }
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}