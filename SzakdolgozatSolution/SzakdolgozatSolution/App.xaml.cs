﻿using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using Szakdolgozat.Core;

namespace Szakdolgozat
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : System.Windows.Application
    {
        private static bool _alt;
        private static Form1 _frm;

        /// <summary>
        ///     Override startup to load IoC before window is created
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            SetupApplication();

            // Wait for server to start
#if DEBUG
            Thread.Sleep(2200);
#endif
            if (File.Exists("CurrentUser.json"))
            {
                var token = new TokenModel("CurrentUser.json");

                if (token.IsValid())
                {
                    IoC.Settings.Token = token.Token;
                    IoC.Settings.UserId = token.UserId;
                    IoC.Settings.UserName = token.Username;
                    IoC.Application.GoToPage(ApplicationPage.Continue, new ContinueViewModel(token.Username));
                }
            }

            KeyboardHook.CreateHook();
            KeyboardHook.OnKeyCapture += KeyboardHook_OnKeyCapture;

            Current.MainWindow = new MainWindow();
            Current.MainWindow.Show();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            KeyboardHook.DeleteHook();
            base.OnExit(e);
        }

        private void KeyboardHook_OnKeyCapture(object sender, KeyCaptureEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(IoC.Settings.UserName)) return;

            if (e.Key == Keys.LMenu || e.Key == Keys.RMenu)
                switch (e.Flags >> 7)
                {
                    // alt key pressed?
                    case 0:
                        _alt = true;
                        break;
                    case 1:
                        _alt = false;
                        break;
                }
            if (!_alt || e.Flags >> 7 != 0 || e.Key != Keys.F3) return; // alt+f3 pressed?
            if (_frm != null && !_frm.IsDisposed) // if already open, close it first
                _frm.Close();
            _frm = new Form1();
            _frm.Show();
        }

        private void SetupApplication()
        {
            // Call IoC setup to bind viewmodels so we can acces them everywhere
            IoC.Setup();

            IoC.Kernel.Bind<IUIManager>().ToConstant(new UIManager());
        }
    }
}