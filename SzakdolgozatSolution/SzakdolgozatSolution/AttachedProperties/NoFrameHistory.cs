﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace Szakdolgozat
{
    /// <summary>
    /// Attached property for frame, to never keep navigation history
    /// </summary>
    public class NoFrameHistory : BaseAttachedProperty<IsBusyProperty, bool>
    {
        public override void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var frame = (sender as Frame);

            frame.NavigationUIVisibility = NavigationUIVisibility.Hidden;

            frame.Navigated += (o, ev) => ((Frame)o).NavigationService.RemoveBackEntry();

        }
    }
}
