﻿namespace Szakdolgozat
{
    /// <summary>
    /// The IsBusy attached property for anything to flag if he control is busy
    /// </summary>
    public class IsBusyProperty : BaseAttachedProperty<IsBusyProperty, bool>
    {

    }
}
