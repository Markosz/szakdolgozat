﻿using System;
using System.Windows;

namespace Szakdolgozat
{
    /// <summary>
    /// A helper class to run any animation when bool is set to true
    /// and a reverse animation when it is set to false
    /// </summary>
    /// <typeparam name="Parent"></typeparam>
    public abstract class AnimateBaseProperty<Parent> : BaseAttachedProperty<Parent, bool>
        where Parent : BaseAttachedProperty<Parent, bool>, new()
    {

        public bool FirstLoad { get; set; } = true;

        public override void OnValueUpdated(DependencyObject sender, object value)
        {
            // If it is not UI element, return
            if (!(sender is FrameworkElement element))
                return;

            // Don't fire if it is the same
            if (sender.GetValue(ValueProperty) == value && !FirstLoad)
                return;

            if (FirstLoad)
            {
                // Create a single event that runs only once
                RoutedEventHandler onLoaded = null;
                onLoaded = (s, e) =>
                {
                    element.Loaded -= onLoaded;

                    // Animate the target element
                    Animate(element, (bool)value);

                    FirstLoad = false;
                };

                element.Loaded += onLoaded;
            }
            else
                Animate(element, (bool)value);

        }

        protected virtual void Animate(FrameworkElement element, bool value)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Animate an UI element to slide in from left
    /// </summary>
    public class AnimateSlideInFromLeftProperty : AnimateBaseProperty<AnimateSlideInFromLeftProperty>
    {
        protected override async void Animate(FrameworkElement element, bool value)
        {
            if (value)
                await element.SlideAndFadeInFromLeftAsync(FirstLoad ? 0 : 0.3f, false);
            else
                await element.SlideAndFadeOutToLeftAsync(FirstLoad ? 0 : 0.3f, false);
        }
    }

    /// <summary>
    /// Animate an UI element to slide down from Top
    /// </summary>
    public class AnimateSlideDownFromTopProperty : AnimateBaseProperty<AnimateSlideDownFromTopProperty>
    {
        protected override async void Animate(FrameworkElement element, bool value)
        {
            if (value)
                await element.SlideDownFromTopAsync(0.3f, false);
            else
                await element.SlideDownFromTopAsync(0.3f, false);
        }
    }

    public class AnimateSlideUpFromBottomProperty : AnimateBaseProperty<AnimateSlideUpFromBottomProperty>
    {
        protected override async void Animate(FrameworkElement element, bool value)
        {
            if (value)
                await element.SlideDownFromTopAsync(1f, false);
            else
            {
                if (FirstLoad)
                    await element.SlideAndFadeOutToBottomAsync(0f, false);
                else
                    await element.SlideAndFadeOutToBottomAsync(0.3f, false);
            }
        }
    }
}