﻿using System;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using Szakdolgozat.Core;

namespace Szakdolgozat
{
    /// <summary>
    ///     Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class EditPage : BasePage<ImageListItemViewModel>
    {
        private Color color = Colors.OrangeRed;
        private Brush pb = Brushes.OrangeRed;
        private Line line;
        private bool lineStarted;
        private EditTool selectEditTool = EditTool.None;

        public EditPage()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            selectEditTool = EditTool.Pen;

            InkCanvas.DefaultDrawingAttributes.Color = color;

            InkCanvas.EditingMode = InkCanvas.EditingMode != InkCanvasEditingMode.Ink
                ? InkCanvasEditingMode.Ink
                : InkCanvasEditingMode.None;
            InkCanvas.UseCustomCursor ^= true;
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            IoC.Application.GoToPage(ApplicationPage.Main, (ImageListItemViewModel)DataContext);
        }

        private void SaveButtonClick(object sender, RoutedEventArgs e)
        {
            var vm = (ImageListItemViewModel)DataContext;

            var renderTargetBitmap = new RenderTargetBitmap((int)Image.ActualWidth, (int)Image.ActualHeight, 96, 96,
                PixelFormats.Default);
            renderTargetBitmap.Render(Image);
            renderTargetBitmap.Render(InkCanvas);

            var encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(renderTargetBitmap));
            var fs = File.Open(vm.Image + 2, FileMode.Create);
            encoder.Save(fs);
            fs.Close();

            var request = (HttpWebRequest)WebRequest.Create("http://127.0.0.1:5000/api/image");
            request.ContentType = "application/json";
            request.Method = "PUT";

            if (request.CookieContainer == null) request.CookieContainer = new CookieContainer(10);
            request.CookieContainer.Add(request.RequestUri, new Cookie("id", IoC.Settings.UserId.ToString()));
            request.CookieContainer.Add(request.RequestUri, new Cookie("token", IoC.Settings.Token));

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                var base64Image = Convert.ToBase64String(File.ReadAllBytes(vm.Image + "2"));

                var data = new ImageModel
                {
                    Title = vm.Caption,
                    Description = vm.Description,
                    Image = base64Image,
                    ImageOwnerId = IoC.Settings.UserId,
                    ImageSize = vm.Image.Length,
                    ImageName = vm.Image.Substring(vm.Image.LastIndexOf('\\') + 1)
                };

                var json = JsonConvert.SerializeObject(data);

                streamWriter.Write(json);
            }

            Page.Image = null;

            var response = (HttpWebResponse)request.GetResponse();

            File.Copy(vm.Image + "2", vm.Image, true);
            File.Delete(vm.Image + "2");
            IoC.Application.GoToPage(ApplicationPage.Main, (ImageListItemViewModel)DataContext);
        }

        private void LineButtonClick(object sender, RoutedEventArgs e)
        {
            InkCanvas.EditingMode = InkCanvasEditingMode.None;
            InkCanvas.UseCustomCursor = false;

            selectEditTool = selectEditTool == EditTool.Line ? EditTool.None : EditTool.Line;
        }

        private void InkCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (selectEditTool == EditTool.Line && lineStarted)
            {
                line.X2 = e.GetPosition(DrawingArea).X;
                line.Y2 = e.GetPosition(DrawingArea).Y;
                InkCanvas.Children.Remove(line);
                InkCanvas.Children.Add(line);
            }
        }

        private void InkCanvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (selectEditTool == EditTool.Line)
            {
                line.X2 = e.GetPosition(DrawingArea).X;
                line.Y2 = e.GetPosition(DrawingArea).Y;
                InkCanvas.Children.Remove(line);
                InkCanvas.Children.Add(line);
                lineStarted = false;
            }
        }

        private void InkCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (selectEditTool == EditTool.Line)
            {
                lineStarted = true;

                line = new Line
                {
                    X1 = e.GetPosition(DrawingArea).X,
                    Y1 = e.GetPosition(DrawingArea).Y,
                    Stroke = pb,
                    StrokeThickness = 2
                };
            }
        }
    }
}