﻿using Szakdolgozat.Core;

namespace Szakdolgozat
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class SharePage : BasePage<ShareViewModel>
    {
        public SharePage()
        {
            InitializeComponent();
        }
    }
}
