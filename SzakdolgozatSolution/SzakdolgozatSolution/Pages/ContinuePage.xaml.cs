﻿using Szakdolgozat.Core;

namespace Szakdolgozat
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class ContinuePage : BasePage<ContinueViewModel>
    {
        public ContinuePage()
        {
            InitializeComponent();
        }
    }
}
