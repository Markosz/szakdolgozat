﻿using System.Windows.Controls;
using Szakdolgozat.Core;

namespace Szakdolgozat
{
    public class BasePage<VM> : Page
        where VM : BaseViewModel, new()
    {
        private VM _viewModel;

        /// <summary>
        /// The view model for this page
        /// </summary>
        public VM ViewModel
        {
            get => _viewModel;
            set
            {
                if (_viewModel == value)
                    return;

                _viewModel = value;

                // Set the data context for this page
                DataContext = _viewModel;
            }
        }

        public BasePage()
        {
            // Create a default view model
            ViewModel = new VM();
        }
    }
}