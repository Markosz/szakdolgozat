﻿using Szakdolgozat.Core;

namespace Szakdolgozat
{

    /// <summary>
    /// Locate view model from IoC for binding in XAML
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Instance of the locator
        /// </summary>
        public static ViewModelLocator Instance { get; private set; } = new ViewModelLocator();

        /// <summary>
        /// Application view model
        /// </summary>
        public ApplicationViewModel ApplicationViewModel => IoC.Application;

        /// <summary>
        /// Settings view model
        /// </summary>
        public SettingsViewModel SettingsViewModel => IoC.Settings;

        // Return a new instance of ImageListItemViewModel
        public UploadViewModel UploadViewModel => IoC.Get<UploadViewModel>();

    }
}
