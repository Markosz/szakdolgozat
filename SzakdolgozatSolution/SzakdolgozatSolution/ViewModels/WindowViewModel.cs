﻿namespace Szakdolgozat
{
    using System.Windows;
    using System.Windows.Input;
    using Core;

    // The main window
    class WindowViewModel : BaseViewModel
    {
        #region Private members
        private Window mWindow;
        #endregion

        #region Public members

        public double MinWindowWidth { get; set; } = 750;

        public double MinWindowHeight { get; set; } = 440;

        public ICommand MinimizeCommand { get; set; }
        public ICommand MaximizeCommand { get; set; }
        public ICommand CloseCommand { get; set; }
        public ICommand PinCommand { get; set; }
        public ICommand TopCommand { get; set; }

        #endregion

        public WindowViewModel(Window window)
        {
            mWindow = window;

            mWindow.StateChanged += (sender, e) =>
            {
                //OnPropertyChanged(nameof(MinWindowWidth));
            };

            // Fix WindowStyle="None" resizing issue
            var resizer = new WindowResizer(mWindow);

            MaximizeCommand = new RelayCommand(() => mWindow.WindowState ^= WindowState.Maximized);
            MinimizeCommand = new RelayCommand(() => mWindow.WindowState = WindowState.Minimized);
            CloseCommand = new RelayCommand(() => mWindow.Close());

            PinCommand = new RelayCommand(Pin);
            TopCommand = new RelayCommand(Top);
        }

        private void Top()
        {
            mWindow.Topmost ^= true;
        }

        private void Pin()
        {
            ((ImagePopoutWindow)mWindow).FixedPosition ^= true;
        }
    }
}