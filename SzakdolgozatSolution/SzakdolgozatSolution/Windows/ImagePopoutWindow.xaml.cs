﻿using System;
using System.Windows;
using System.Windows.Interop;

namespace Szakdolgozat
{
    /// <summary>
    ///     Interaction logic for ImagePopoutWindow.xaml
    /// </summary>
    public partial class ImagePopoutWindow : Window
    {
        public ImagePopoutWindow()
        {
            InitializeComponent();

            DataContext = new WindowViewModel(this);

            MaxWidth = SystemParameters.PrimaryScreenWidth;
            MaxHeight = SystemParameters.PrimaryScreenHeight;
        }

        public bool FixedPosition { get; set; }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (!FixedPosition) return IntPtr.Zero;

            switch (msg)
            {
                case 0x0112:
                    var command = wParam.ToInt32() & 0xfff0;
                    if (command == 0xF010)
                        // prevent user from moving the window
                        handled = true;
                    break;
            }

            return IntPtr.Zero;
        }

        private void ImagePopoutWindow_OnSourceInitialized(object sender, EventArgs e)
        {
            var helper = new WindowInteropHelper(this);
            var source = HwndSource.FromHwnd(helper.Handle);
            source.AddHook(WndProc);
        }
    }
}