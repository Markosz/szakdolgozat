﻿using System.Threading.Tasks;
using System.Windows;
using Szakdolgozat.Core;

namespace Szakdolgozat
{
    public class UIManager : IUIManager
    {
        public Task ShowMessage(MessageBoxViewModel viewmodel)
        {
            return Task.Run(() => MessageBox.Show(viewmodel.Message, viewmodel.Title));
        }
    }
}
