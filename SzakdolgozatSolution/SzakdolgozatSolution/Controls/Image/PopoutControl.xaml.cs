﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Szakdolgozat.Core;

namespace Szakdolgozat
{
    /// <summary>
    ///     Interaction logic for ImageControl.xaml
    /// </summary>
    public partial class PopoutControl : UserControl
    {
        private bool once;
        private readonly Transformations transform;

        public PopoutControl()
        {
            InitializeComponent();

            transform = new Transformations();
            transform.AddTransformation(image, border);
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2) transform.Reset();
        }

        private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (once) return;

            once = true;
            DataContext = new ImageListItemViewModel
            {
                Image = (DataContext as ImageListItemViewModel)?.Image
            };
        }
    }
}