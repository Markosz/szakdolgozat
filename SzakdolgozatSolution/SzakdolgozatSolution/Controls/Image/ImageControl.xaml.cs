﻿using System.Windows;
using System.Windows.Controls;
using Szakdolgozat.Core;

namespace Szakdolgozat
{
    /// <summary>
    ///     Interaction logic for ImageControl.xaml
    /// </summary>
    public partial class ImageControl : UserControl
    {
        private Transformations transform;

        public ImageControl()
        {
            InitializeComponent();

            transform = new Transformations();
            transform.AddTransformation(image, border);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (IoC.Application.CurrentPageViewModel is ImageListItemViewModel model)
            {
                IoC.Application.PopoutViewModel = model;
            }

            var pWindow = new ImagePopoutWindow();
            pWindow.Show();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            transform.Reset();
        }
    }
}