﻿using System.Windows.Controls;
using Szakdolgozat.Core;

namespace Szakdolgozat
{
    /// <summary>
    /// Interaction logic for ImageListControl.xaml
    /// </summary>
    public partial class ImageListControl : UserControl
    {
        public ImageListControl(ImageListViewModel vm)
        {
            InitializeComponent();

            DataContext = vm;
        }
    }
}