﻿using System.Windows.Controls;
using Szakdolgozat.Core;

namespace Szakdolgozat
{
    /// <summary>
    /// Interaction logic for SettingsControl.xaml
    /// </summary>
    public partial class UploadControl : UserControl
    {
        public UploadControl()
        {
            InitializeComponent();

            DataContext = IoC.Get<UploadViewModel>();
        }
    }
}
