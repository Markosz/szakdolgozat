﻿using System;
using System.Windows.Input;

namespace SzakdolgozatSolution
{
    public class RelayCommand : ICommand
    {
        /// <summary>
        /// Action to run
        /// </summary>
        private Action mAction;

        public event EventHandler CanExecuteChanged = (sender, e) => { };

        /// <summary>
        /// Always execute
        /// </summary>
        public bool CanExecute(object parameter) => true;

        public RelayCommand(Action action)
        {
            mAction = action;
        }

        public void Execute(object parameter)
        {
            mAction();
        }
    }
}
