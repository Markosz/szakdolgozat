﻿using System.Security;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SzakdolgozatSolution
{
    public class LoginViewModel : BaseViewModel
    {
        #region Public properties

        public string Email { get; set; }

        public bool LoginRunning { get; set; }

        #endregion

        #region Commands

        public ICommand LoginCommand { get; set; }

        #endregion

        public LoginViewModel()
        {
            LoginCommand = new RelayParameterizedCommand(async (parameter) => await Login(parameter));
        }

        /// <summary>
        /// Logs in
        /// </summary>
        /// <param name="parameter">The secure string from password TextBox</param>
        /// <returns></returns>
        public async Task Login(object parameter)
        {
            await RunCommand(() => this.LoginRunning, async () =>
            {
                // Come on, do something!
                await Task.Delay(1000);
                var email = this.Email;
            });
        }
    }
}
