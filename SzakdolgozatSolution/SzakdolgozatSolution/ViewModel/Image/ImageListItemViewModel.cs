﻿namespace SzakdolgozatSolution
{
    /// <summary>
    /// View model for each image list item in the sidebar
    /// </summary>
    public class ImageListItemViewModel : BaseViewModel
    {
        public string Caption { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        public bool IsSelected { get; set; } = false;
    }
}
