﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SzakdolgozatSolution
{
    /// <summary>
    /// View model for the image list in the sidebar
    /// </summary>
    public class ImageListViewModel : BaseViewModel
    {
        public List<ImageListItemViewModel> Items { get; set; }
    }
}
