﻿using System;
using System.Windows.Input;

namespace SzakdolgozatSolution
{
    public class RelayParameterizedCommand : ICommand
    {
        /// <summary>
        /// Action to run
        /// </summary>
        private Action<object> mAction;

        public event EventHandler CanExecuteChanged = (sender, e) => { };

        /// <summary>
        /// Always execute
        /// </summary>
        public bool CanExecute(object parameter) => true;

        public RelayParameterizedCommand(Action<object> action)
        {
            mAction = action;
        }

        public void Execute(object parameter)
        {
            mAction(parameter);
        }
    }
}
