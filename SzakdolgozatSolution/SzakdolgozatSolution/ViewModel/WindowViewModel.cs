﻿namespace SzakdolgozatSolution
{
    using System.Windows;
    using System.Windows.Input;

    // The main window
    class WindowViewModel : BaseViewModel
    {
        #region Private members
        private Window mWindow;
        #endregion

        #region Public members

        public double MinWindowWidth { get; set; } = 750;

        public double MinWindowHeight { get; set; } = 430;

        public ICommand MinimizeCommand { get; set; }

        public ICommand MaximizeCommand { get; set; }

        public ICommand CloseCommand { get; set; }

        /// <summary>
        /// Which page will be displayed. Default is Login.
        /// </summary>
        public ApplicationPage CurrentPage { get; set; } = ApplicationPage.Main;
        #endregion

        public WindowViewModel(Window window)
        {
            mWindow = window;

            mWindow.StateChanged += (sender, e) =>
            {
                //OnPropertyChanged(nameof(MinWindowWidth));
            };

            // Fix WindowStyle="None" resizing issue
            var resizer = new WindowResizer(mWindow);

            MaximizeCommand = new RelayCommand(() => mWindow.WindowState ^= WindowState.Maximized);
            MinimizeCommand = new RelayCommand(() => mWindow.WindowState = WindowState.Minimized);
            CloseCommand = new RelayCommand(() => mWindow.Close());
        }
    }
}