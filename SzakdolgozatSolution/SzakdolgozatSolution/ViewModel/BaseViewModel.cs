﻿namespace SzakdolgozatSolution
{
    using PropertyChanged;
    using System;
    using System.ComponentModel;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

        public void OnPropertyChanged(string name)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Runs a command if it is not already running
        /// </summary>
        /// <param name="updateingFlag">Flag is indicating if the function is already running</param>
        /// <param name="action">The action to run when it is not already running</param>
        /// <returns></returns>
        protected async Task RunCommand(Expression<Func<bool>> updateingFlag, Func<Task> action)
        {
            if (updateingFlag.GetPropertyValue())
                return;

            // Set the running flag to true
            updateingFlag.SetPropertyValue(true);

            try
            {
                await action();
            }
            finally
            {
                updateingFlag.SetPropertyValue(false);
            }
        }
    }
}