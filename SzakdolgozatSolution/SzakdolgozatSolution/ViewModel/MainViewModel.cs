﻿namespace SzakdolgozatSolution
{
    public class MainViewModel : BaseViewModel
    {
        public ImageListItemViewModel Selected { get; set; }

        public MainViewModel()
        {
            Selected = new ImageListItemViewModel
            {
                Caption = "Szisza",
                Description = "This is a testű",
                Image = "G:\\Szakdolgozat\\Resources\\Image\\testest.jpg"
            };
        }
    }
}
