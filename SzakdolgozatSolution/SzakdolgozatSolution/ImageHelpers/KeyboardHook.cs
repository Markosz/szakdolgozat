﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Szakdolgozat
{
    public static class KeyboardHook
    {
        public delegate void KeyCaptureHandler(object sender, KeyCaptureEventArgs e);

        private static LowLevelKeyboardProc _objKeyboardProcess;
        private static IntPtr _ptrHook;
        public static event KeyCaptureHandler OnKeyCapture;

        public static void CreateHook()
        {
            var objCurrentModule = Process.GetCurrentProcess().MainModule;
            _objKeyboardProcess = KeyCapture;
            _ptrHook = SetWindowsHookEx(13, _objKeyboardProcess, GetModuleHandle(objCurrentModule.ModuleName), 0);
        }

        public static void DeleteHook()
        {
            if (_ptrHook == IntPtr.Zero) return;
            UnhookWindowsHookEx(_ptrHook);
            _ptrHook = IntPtr.Zero;
        }

        private static IntPtr KeyCapture(int nCode, IntPtr wp, IntPtr lp)
        {
            if (OnKeyCapture == null) return CallNextHookEx(_ptrHook, nCode, wp, lp);
            if (nCode < 0) return CallNextHookEx(_ptrHook, nCode, wp, lp);
            var objKeyInfo = (Kbdllhookstruct)Marshal.PtrToStructure(lp, typeof(Kbdllhookstruct));
            var e = new KeyCaptureEventArgs
            {
                Key = objKeyInfo.key,
                Extra = objKeyInfo.extra,
                Flags = objKeyInfo.flags,
                ScanCode = objKeyInfo.scanCode,
                Time = objKeyInfo.time
            };
            OnKeyCapture(null, e);
            return CallNextHookEx(_ptrHook, nCode, wp, lp);
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int id, LowLevelKeyboardProc callback, IntPtr hMod,
            uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool UnhookWindowsHookEx(IntPtr hook);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hook, int nCode, IntPtr wp, IntPtr lp);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string name);

        [StructLayout(LayoutKind.Sequential)]
        private struct Kbdllhookstruct
        {
            public readonly Keys key;
            public readonly int scanCode;
            public readonly int flags;
            public readonly int time;
            public readonly IntPtr extra;
        }

        private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);
    }

    public class KeyCaptureEventArgs
    {
        public IntPtr Extra;
        public int Flags;
        public Keys Key;
        public int ScanCode;
        public int Time;
    }
}