﻿using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace Szakdolgozat
{
    public class Transformations
    {
        private UIElement _border;
        private UIElement _image;
        private Point _origin;
        private Point _start;

        public void AddTransformation(UIElement target, UIElement borderr)
        {
            _image = target;
            _border = borderr;
            var group = new TransformGroup();

            var xform = new ScaleTransform();
            group.Children.Add(xform);

            var tt = new TranslateTransform();
            group.Children.Add(tt);

            _image.RenderTransform = group;

            _image.MouseWheel += image_MouseWheel;
            _image.MouseLeftButtonDown += image_MouseLeftButtonDown;
            _image.MouseLeftButtonUp += image_MouseLeftButtonUp;
            _image.MouseMove += image_MouseMove;
        }

        public void image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _image.ReleaseMouseCapture();
        }

        public void image_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_image.IsMouseCaptured) return;

            var translateTransform = (TranslateTransform)((TransformGroup)_image.RenderTransform).Children.First(tr =>
              tr is TranslateTransform);

            var v = _start - e.GetPosition(_border);

            translateTransform.X = _origin.X - v.X;
            translateTransform.Y = _origin.Y - v.Y;
        }

        public void image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _image.CaptureMouse();
            var translateTransform = (TranslateTransform)((TransformGroup)_image.RenderTransform).Children.First(tr =>
              tr is TranslateTransform);

            _start = e.GetPosition(_border);
            _origin = new Point(translateTransform.X, translateTransform.Y);
        }

        public void image_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            var transformGroup = (TransformGroup)_image.RenderTransform;
            var scaleTransform = (ScaleTransform)transformGroup.Children[0];

            var zoom = e.Delta > 0 ? .1 : -.1;
            if (scaleTransform.ScaleX <= 0.2 && e.Delta < 0) return;

            scaleTransform.ScaleX += zoom;
            scaleTransform.ScaleY += zoom;
        }

        /// <summary>
        /// Reset every transformation
        /// </summary>
        public void Reset()
        {
            var transformGroup = (TransformGroup)_image.RenderTransform;
            var scaleTransform = (ScaleTransform)transformGroup.Children[0];
            var translateTransform = (TranslateTransform)((TransformGroup)_image.RenderTransform).Children.First(tr =>
                tr is TranslateTransform);

            scaleTransform.ScaleX = 1;
            scaleTransform.ScaleY = 1;

            translateTransform.X = 0;
            translateTransform.Y = 0;
        }
    }
}