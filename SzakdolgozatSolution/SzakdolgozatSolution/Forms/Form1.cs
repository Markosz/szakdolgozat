﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Szakdolgozat.Core;
using Szakdolgozat.Properties;

namespace Szakdolgozat
{
    public partial class Form1 : Form
    {
        private Bitmap _bmp;
        private bool _drawing;
        private Graphics _grap;
        private Point _lastPoint = Point.Empty;
        private bool _mDown;
        private Point _mPoint = Point.Empty;
        private Point _mPress = Point.Empty;
        private Point _ptDown = Point.Empty;
        private bool _selecting;

        public Form1()
        {
            InitializeComponent();
            _selecting = true;
            _drawing = false;

            GetScreen();

            flowLayoutPanel1.BackColor = Color.FromArgb(128, 0, 0, 0);
            flowLayoutPanel1.Top = 980;
            flowLayoutPanel1.Left = Width / 2 - 210;

            AddKeyDownForward(this);
        }

        /// <summary>
        /// Fix for screen flickering.
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                var cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        /// <summary>
        /// Find the screen where the cursor is
        /// </summary>
        private void GetScreen()
        {
            var screen = Screen.AllScreens.FirstOrDefault(s => // Finding the screen
                Cursor.Position.X >= s.Bounds.Left &&
                Cursor.Position.X <= s.Bounds.Right &&
                Cursor.Position.Y >= s.Bounds.Top &&
                Cursor.Position.Y <= s.Bounds.Bottom);

            if (screen == null)
            {
                Close();
                return;
            }

            _bmp = new Bitmap(screen.Bounds.Width, screen.Bounds.Height, PixelFormat.Format32bppRgb);
            _grap = Graphics.FromImage(_bmp);
            _grap.CopyFromScreen(screen.Bounds.Left, screen.Bounds.Top, 0, 0, screen.Bounds.Size);

            Width = _bmp.Width;
            Height = _bmp.Height;
            Left = screen.Bounds.Left;
            Top = screen.Bounds.Top;
        }

        private void DrawControls()
        {
            if (_mPoint.IsEmpty)
                _ptDown = Point.Empty;

            if (Math.Abs(_mPoint.Y - _ptDown.Y) > 0) // Control window positioning
            {
                if (Math.Max(_mPoint.Y, _ptDown.Y) <= Height - 100) // Check bottom
                    flowLayoutPanel1.Top = Math.Min(_mPoint.Y, _ptDown.Y) + Math.Abs(_mPoint.Y - _ptDown.Y) + 15;
                else
                    flowLayoutPanel1.Top = Math.Max(_mPoint.Y, _ptDown.Y) - Math.Abs(_mPoint.Y - _ptDown.Y) - 75;
                flowLayoutPanel1.Left = Math.Min(_mPoint.X, _ptDown.X) + Math.Abs(_mPoint.X - _ptDown.X) / 2 - 210;
                WhereIsControlBox();
            }
            else
            {
                flowLayoutPanel1.Top = Screen.PrimaryScreen.Bounds.Bottom - 100;
                flowLayoutPanel1.Left = Width / 2 - 210;
            }

            flowLayoutPanel1.Visible = true;
        }

        private void WhereIsControlBox() // is it out of screen?
        {
            if (flowLayoutPanel1.Left <= 0)
                flowLayoutPanel1.Left = 0;
            if (flowLayoutPanel1.Right >= Width)
                flowLayoutPanel1.Left = Width - flowLayoutPanel1.Width;
            if (flowLayoutPanel1.Top + 60 >= Height)
                flowLayoutPanel1.Top = Height - 60;
            if (flowLayoutPanel1.Top <= 0)
                flowLayoutPanel1.Top = 0;
        }

        private void AddKeyDownForward(Control ctrl)
        {
            ctrl.KeyDown += OnKeyDown;
            foreach (Control c in ctrl.Controls)
                AddKeyDownForward(c);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.DrawImage(_bmp, 0, 0, Width, Height);
            var r = new Region(ClientRectangle);

            if (_ptDown.IsEmpty && _mPoint.IsEmpty)
                if (!_drawing)
                    e.Graphics.FillRegion(new SolidBrush(Color.FromArgb(133, 255, 255, 255)), r);

            if (_ptDown.IsEmpty && _mPoint.IsEmpty) return;
            var window = new System.Drawing.Rectangle(
                Math.Min(_ptDown.X, _mPoint.X),
                Math.Min(_ptDown.Y, _mPoint.Y),
                Math.Abs(_ptDown.X - _mPoint.X),
                Math.Abs(_ptDown.Y - _mPoint.Y));

            e.Graphics.DrawRectangle(Pens.Crimson, window);
            r.Exclude(window);
            e.Graphics.FillRegion(new SolidBrush(Color.FromArgb(133, 255, 255, 255)), r);
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                if (_drawing)
                {
                    _drawing = false;
                    Cursor = DefaultCursor;
                    Invalidate();
                }
                else
                {
                    Close();
                }

            if (e.KeyCode == Keys.S && e.Control)
            {
                Save();
            }

            if (e.KeyCode == Keys.C && e.Control)
            {
                if (_ptDown.IsEmpty || _mPoint.IsEmpty)
                {
                    Clipboard.SetImage(_bmp);
                }
                else
                {
                    int w = Math.Abs(_mPoint.X - _ptDown.X), h = Math.Abs(_mPoint.Y - _ptDown.Y);
                    var rbmp = new Bitmap(w, h, PixelFormat.Format32bppRgb);
                    var grp = Graphics.FromImage(rbmp);
                    grp.DrawImage(_bmp, new System.Drawing.Rectangle(0, 0, w, h),
                        new System.Drawing.Rectangle(Math.Min(_mPoint.X, _ptDown.X), Math.Min(_mPoint.Y, _ptDown.Y), w, h),
                        GraphicsUnit.Pixel);
                    Clipboard.SetImage(rbmp);
                }

                Close();
            }
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            _mDown = true;

            if (_drawing)
                _lastPoint = e.Location;

            if (!_selecting) return;
            flowLayoutPanel1.Visible = false;
            _ptDown = e.Location;
            _mPoint = Point.Empty;
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            _mDown = false;
            if (_selecting)
                DrawControls();
            if (!_drawing)
                Cursor = DefaultCursor;
            _selecting = false;
            Invalidate();
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_mDown || e.Button != MouseButtons.Left) return;

            if (_drawing) // FREE HAND DRAWING
            {
                _mPress = e.Location;
                _grap.SmoothingMode = SmoothingMode.AntiAlias;
                _grap.DrawLine(new Pen(Color.DarkRed, 3), _mPress, _lastPoint); // MAKE PEN VAR
                _lastPoint = e.Location;
            }

            if (_selecting)
                _mPoint = e.Location;
            Invalidate();
        }


        private void Button1_Click(object sender, EventArgs e)
        {
            flowLayoutPanel1.Visible = false;
            _mPoint = Point.Empty;
            _ptDown = Point.Empty;
            _mPress = Point.Empty;
            Cursor = Cursors.Cross;
            _selecting = true;
            _drawing = false;
            Invalidate();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            _grap.Dispose();
            _bmp.Dispose();
        }

        private void FlowLayoutPanel1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                _mPress = new Point(e.X, e.Y);
        }

        private void FlowLayoutPanel1_MouseMove(object sender, MouseEventArgs e) // Control panel dragging
        {
            if (e.Button != MouseButtons.Left) return;
            flowLayoutPanel1.Left += e.X - _mPress.X;
            flowLayoutPanel1.Top += e.Y - _mPress.Y;
            WhereIsControlBox();
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Button2_Click(object sender, EventArgs e) // PEN
        {
            _drawing = true;
            _selecting = false;
            Cursor = new Cursor(new MemoryStream(Resources.aero_pen));
        }

        private void Popout_Click(object sender, EventArgs e)
        {
            int w = Math.Abs(_mPoint.X - _ptDown.X), h = Math.Abs(_mPoint.Y - _ptDown.Y);
            var rbmp = new Bitmap(w, h, PixelFormat.Format32bppRgb);
            var grp = Graphics.FromImage(rbmp);
            grp.DrawImage(_bmp, new System.Drawing.Rectangle(0, 0, w, h),
                new System.Drawing.Rectangle(Math.Min(_mPoint.X, _ptDown.X), Math.Min(_mPoint.Y, _ptDown.Y), w, h),
                GraphicsUnit.Pixel);

            rbmp.Save("temp.jpeg");

            var test = new ImageListItemViewModel
            {
                Image = "temp.jpeg"
            };

            IoC.Application.PopoutViewModel = test;

            var pWindow = new ImagePopoutWindow();
            pWindow.Show();

            File.Delete("temp.jpeg");
            //var test = new Screenshow(rbmp, _ptDown.X, _ptDown.Y);
            //test.Show();

            Close();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void Save()
        {
            int w = Math.Abs(_mPoint.X - _ptDown.X), h = Math.Abs(_mPoint.Y - _ptDown.Y);
            var rbmp = new Bitmap(w, h, PixelFormat.Format32bppRgb);
            var grp = Graphics.FromImage(rbmp);
            grp.DrawImage(_bmp, new System.Drawing.Rectangle(0, 0, w, h),
                new System.Drawing.Rectangle(Math.Min(_mPoint.X, _ptDown.X), Math.Min(_mPoint.Y, _ptDown.Y), w, h),
                GraphicsUnit.Pixel);

            var sfd = new SaveFileDialog
            {
                Filter = @"JPEG Image (.jpeg)|*.jpeg",
                FilterIndex = 1
            };

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                var myStream = sfd.OpenFile();
                rbmp.Save(myStream, ImageFormat.Bmp);
                myStream.Close();
            }

            Close();
        }
    }
}