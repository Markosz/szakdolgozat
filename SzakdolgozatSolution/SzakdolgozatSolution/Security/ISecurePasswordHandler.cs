﻿using System.Security;

namespace SzakdolgozatSolution
{
    public interface ISecurePasswordHandler
    {
        SecureString SecurePassword { get; }
    }
}
