﻿namespace Szakdolgozat
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Navigation;
    using Szakdolgozat.Core;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new WindowViewModel(this);
        }

        public RegisterPage RegisterPage
        {
            get => default;
            set
            {
            }
        }

        public LoginPage LoginPage
        {
            get => default;
            set
            {
            }
        }

        public MainPage MainPage
        {
            get => default;
            set
            {
            }
        }

        private void MainFrame_Navigated(object sender, NavigationEventArgs e)
        {
            var frame = (sender as Frame);

            frame.NavigationUIVisibility = NavigationUIVisibility.Hidden;

            frame.NavigationService.RemoveBackEntry();
        }
    }
}