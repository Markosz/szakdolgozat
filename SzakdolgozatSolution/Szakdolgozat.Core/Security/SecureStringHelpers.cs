﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Szakdolgozat.Core
{
    /// <summary>
    /// Helpers for the <see cref="SecureString"/> class
    /// </summary>
    public static class SecureStringHelpers
    {
        // Shows the secure password as plain text - not secure anymore
        public static string Unsecure(this SecureString securePassword)
        {
            if (securePassword == null)
                return string.Empty;

            // Pointer for the string in memory
            var unmanagedString = IntPtr.Zero;

            try
            {
                // Unsecures the password
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(securePassword);
                return Marshal.PtrToStringUni(unmanagedString);
            }
            finally
            {
                // Clean up memory allocation
                Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }
    }
}