﻿using System.Security;

namespace Szakdolgozat.Core
{
    public interface ISecurePasswordHandler
    {
        SecureString SecurePassword { get; }
    }
}
