﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace Szakdolgozat.Core
{
    public class TokenModel
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
        public DateTime Validity { get; set; }

        public TokenModel()
        {
        }

        public TokenModel(string path)
        {
            var previousUser = JsonConvert.DeserializeObject<TokenModel>(File.ReadAllText(path));

            UserId = previousUser.UserId;
            Username = previousUser.Username;
            Token = previousUser.Token;
            Validity = previousUser.Validity;
        }

        public TokenModel(int userId, string username, string token, DateTime validity)
        {
            UserId = userId;
            Username = username;
            Token = token;
            Validity = validity;
        }


        public void WriteToFile()
        {
            File.WriteAllText("CurrentUser.json", JsonConvert.SerializeObject(this));
        }

        public bool IsValid()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://127.0.0.1:5000/api/auth");
            request.ContentType = "application/json";
            request.Method = "POST";

            if (request.CookieContainer == null)
            {
                request.CookieContainer = new CookieContainer(10);
            }

            try
            {
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    string json = JsonConvert.SerializeObject(this);
                    streamWriter.Write(json);
                }

                var response = (HttpWebResponse)request.GetResponse();

                if (response.Cookies["success"].Value == "true")
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
