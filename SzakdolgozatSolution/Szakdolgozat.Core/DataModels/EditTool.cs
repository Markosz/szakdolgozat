﻿namespace Szakdolgozat.Core
{
    public enum EditTool
    {
        None = 0,

        Pen = 1,

        Line = 2,
    }
}
