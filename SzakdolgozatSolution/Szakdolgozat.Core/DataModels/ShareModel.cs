﻿namespace Szakdolgozat.Core
{
    public class ShareModel
    {
        public int ImageID { get; set; }
        public int ImageOwnerId { get; set; }
        public string ShareTo { get; set; }
    }
}
