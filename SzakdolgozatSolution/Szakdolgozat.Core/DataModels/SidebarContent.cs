﻿namespace Szakdolgozat.Core
{
    public enum SidebarContent
    {

        // The user's images
        Images = 1,
        // Shared images with the user
        Shared = 2
    }
}
