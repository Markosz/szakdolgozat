﻿namespace Szakdolgozat.Core
{
    public class ImageModel
    {
        public int ImageID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string ImageName { get; set; }
        public double ImageSize { get; set; }
        public int ImageOwnerId { get; set; }
    }
}
