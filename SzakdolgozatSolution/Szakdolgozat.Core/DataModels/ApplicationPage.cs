﻿namespace Szakdolgozat.Core
{
    public enum ApplicationPage
    {
        Login = 0,

        Main = 1,

        Register = 2,

        Continue = 3,

        Popout = 4,

        Edit = 5,

        Share = 6,
    }
}
