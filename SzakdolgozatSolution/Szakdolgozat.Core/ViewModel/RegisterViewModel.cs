﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;

namespace Szakdolgozat.Core
{
    public class RegisterViewModel : BaseViewModel
    {
        public RegisterViewModel()
        {
            RegisterCommand = new RelayParameterizedCommand(async parameter => await RegisterAsync(parameter));
            CancelCommand = new RelayParameterizedCommand(parameter => Cancel());
        }

        public string Username { get; set; }
        public string Email { get; set; }
        public string Response { get; set; }

        public bool RegisterRunning { get; set; }

        private void Cancel()
        {
            IoC.Application.CurrentPage = ApplicationPage.Login;
        }

        private async Task RegisterAsync(object parameter = null)
        {
            if (string.IsNullOrWhiteSpace(Username) || Username.Length < 4)
            {
                Response = "Username is too short.";
                return;
            }

            try
            {
                var test = new MailAddress(Email);
            }
            catch (ArgumentNullException)
            {
                Response = "Email is missing.";
                return;
            }
            catch (FormatException)
            {
                Response = "Invalid Email format.";
                return;
            }

            if ((parameter as ISecurePasswordHandler).SecurePassword?.Length < 6)
            {
                Response = "Password is too short.";
                return;
            }

            await RunCommandAsync(() => RegisterRunning, async () =>
            {
                var request = (HttpWebRequest)WebRequest.Create("http://127.0.0.1:5000/api/register");
                request.ContentType = "application/json";
                request.Method = "POST";

                try
                {
                    using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                    {
                        var json = JsonConvert.SerializeObject(new
                        {
                            email = $"{Email.ToLower()}",
                            username = $"{Username}",
                            password = $"{(parameter as ISecurePasswordHandler).SecurePassword.Unsecure()}"
                        });

                        streamWriter.Write(json);
                    }

                    var response = await request.GetResponseAsync();


                    using (var sr = new StreamReader(response.GetResponseStream()))
                    {
                        Response = sr.ReadToEnd();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Response = "Server is not responding";
                    return;
                }
            });
        }

        #region Commands

        public ICommand RegisterCommand { get; set; }
        public ICommand CancelCommand { get; set; }

        #endregion
    }
}