﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Win32;
using Newtonsoft.Json;

namespace Szakdolgozat.Core
{
    public class UploadViewModel : BaseViewModel
    {
        public UploadViewModel()
        {
            SaveCommand = new RelayParameterizedCommand(async parameter => await SaveAsync(parameter));
            OpenCommand = new RelayCommand(Open);
            SelectCommand = new RelayCommand(Select);
            CloseCommand = new RelayCommand(Close);
        }

        public string Caption { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public bool SaveRunning { get; set; }
        public string Message { get; set; }

        public ICommand CloseCommand { get; set; }
        public ICommand OpenCommand { get; set; }
        public ICommand SaveCommand { get; set; }
        public ICommand SelectCommand { get; set; }

        private void Select()
        {
            var dlg = new OpenFileDialog
            {
                DefaultExt = ".png",
                Filter =
                    "JPG Files (*.jpg)|*.jpg|JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png"
            };

            var result = dlg.ShowDialog();

            if (result == true)
                Path = dlg.FileName;
        }

        private async Task SaveAsync(object parameter)
        {
            if (IoC.Settings.UserName == "Offline")
            {
                Message = "You must be logged in to upload.";
                return;
            }

            if (string.IsNullOrWhiteSpace(Caption))
            {
                Message = "Enter a title.";
                return;
            }

            if (!File.Exists(Path) || !Path.Contains(".png") && !Path.Contains(".jpg") && !Path.Contains(".jpeg"))
            {
                Message = "You must enter a valid image Path.";
                return;
            }

            await RunCommandAsync(() => SaveRunning, async () =>
            {
                var request = (HttpWebRequest)WebRequest.Create("http://127.0.0.1:5000/api/image");
                request.ContentType = "application/json";
                request.Method = "POST";

                if (request.CookieContainer == null) request.CookieContainer = new CookieContainer(10);
                request.CookieContainer.Add(request.RequestUri, new Cookie("id", IoC.Settings.UserId.ToString()));
                request.CookieContainer.Add(request.RequestUri, new Cookie("token", IoC.Settings.Token));
                var base64Image = Convert.ToBase64String(File.ReadAllBytes(Path));

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    var data = new ImageModel
                    {
                        Title = Caption,
                        Description = Description,
                        Image = base64Image,
                        ImageName = Path.Substring(Path.LastIndexOf('\\') + 1),
                        ImageSize = new FileInfo(Path).Length,
                        ImageOwnerId = IoC.Settings.UserId
                    };

                    var json = JsonConvert.SerializeObject(data);

                    streamWriter.Write(json);
                }

                try
                {
                    var response = (HttpWebResponse)request.GetResponseAsync().Result;

                    using (var sr = new StreamReader(response.GetResponseStream()))
                    {
                        Message = sr.ReadToEnd();
                    }

                    await IoC.Settings.GetImagesAsync();
                }
                catch (Exception e)
                {
                    Message = e.Message;
                }
            });

            if (Message == "Upload complete.")
            {
                Caption = "";
                Description = "";
                Path = "";
                Message = "";
                IoC.Application.UploadMenuVisible = false;
            }
        }

        private void Open()
        {
            IoC.Application.UploadMenuVisible = true;
        }

        private void Close()
        {
            IoC.Application.UploadMenuVisible = false;
        }
    }
}