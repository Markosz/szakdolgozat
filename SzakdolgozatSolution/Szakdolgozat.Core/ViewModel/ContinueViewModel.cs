﻿using System.IO;
using System.Windows.Input;

namespace Szakdolgozat.Core
{
    public class ContinueViewModel : BaseViewModel
    {
        public ContinueViewModel()
        {
            ContinueCommand = new RelayCommand(Continue);
            LoginCommand = new RelayCommand(Login);
        }

        public ContinueViewModel(string user)
        {
            ContinueCommand = new RelayCommand(Continue);
            LoginCommand = new RelayCommand(Login);
            UserName = user;

            Message = $"Welcome back {user}";
        }

        public string UserName { get; set; }
        public string Message { get; set; }
        public bool LoginRunning { get; set; }

        public ICommand ContinueCommand { get; set; }
        public ICommand LoginCommand { get; set; }

        public async void Continue()
        {
            await RunCommandAsync(() => LoginRunning, async () =>
            {
                // Login to the app
                IoC.Application.SideMenuVisible ^= true;
                IoC.Application.CurrentPage = ApplicationPage.Main;

                await IoC.Settings.GetImagesAsync();
                await IoC.Settings.GetSharedAsync();
            });
        }

        public void Login()
        {
            File.Delete("CurrentUser.json");
            Directory.Delete("Images", true);
            IoC.Application.CurrentPage = ApplicationPage.Login;
        }
    }
}