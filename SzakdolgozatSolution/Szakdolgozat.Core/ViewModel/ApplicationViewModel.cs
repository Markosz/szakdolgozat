﻿using System.Windows.Input;

namespace Szakdolgozat.Core
{
    public class ApplicationViewModel : BaseViewModel
    {
        public ApplicationViewModel()
        {
            OpenImagesCommand = new RelayCommand(OpenImages);
            OpenSharedCommand = new RelayCommand(OpenShared);
        }

        public ICommand OpenImagesCommand { get; set; }
        public ICommand OpenSharedCommand { get; set; }

        private void OpenImages()
        {
            CurrentSidebarContent = SidebarContent.Images;
        }

        private void OpenShared()
        {
            CurrentSidebarContent = SidebarContent.Shared;
        }

        public void OpenPopout(ApplicationPage page, BaseViewModel viewModel = null)
        {
            // Set the view model
            PopoutViewModel = viewModel;

            // If we are on the same page, don't change
            if (PopoutPage == page) return;

            // Set the current page
            PopoutPage = page;

            // Force a changed event manually
            OnPropertyChanged(nameof(PopoutPage));
        }

        /// <summary>
        ///     Change to a new page with the selected view model
        /// </summary>
        /// <param name="page"></param>
        /// <param name="viewModel"></param>
        public void GoToPage(ApplicationPage page, BaseViewModel viewModel = null)
        {
            SettingsMenuVisible = false;

            // Set the view model
            CurrentPageViewModel = viewModel;

            // If we are on the same page, don't change
            if (CurrentPage == page) return;

            // Set the current page
            CurrentPage = page;

            // Force a changed event manually
            OnPropertyChanged(nameof(CurrentPage));

            // Show side menu or not?
            SideMenuVisible = page == ApplicationPage.Main;
        }

        #region Public Properties

        /// <summary>
        ///     Which page will be displayed. Default is Login.
        /// </summary>
        public ApplicationPage CurrentPage { get; set; } = ApplicationPage.Login;
        public ApplicationPage PopoutPage { get; set; } = ApplicationPage.Popout;
        public SidebarContent CurrentSidebarContent { get; set; } = SidebarContent.Images;
        public string CurrentUser { get; set; } = "Offline";
        public BaseViewModel CurrentPageViewModel { get; set; }
        public BaseViewModel PopoutViewModel { get; set; }
        public bool SideMenuVisible { get; set; }
        public bool SettingsMenuVisible { get; set; }
        public bool UploadMenuVisible { get; set; }

        #endregion
    }
}