﻿namespace Szakdolgozat.Core
{
    using System;
    using System.ComponentModel;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

        public void OnPropertyChanged(string name)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Runs a command if it is not already running
        /// </summary>
        /// <param name="updateingFlag">Flag is indicating if the function is already running</param>
        /// <param name="action">The action to run when it is not already running</param>
        /// <returns></returns>
        protected async Task RunCommandAsync(Expression<Func<bool>> updateingFlag, Func<Task> action)
        {
            if (updateingFlag.GetPropertyValue())
                return;

            // Set the running flag to true
            updateingFlag.SetPropertyValue(true);

            try
            {
                await action();
            }
            finally
            {
                updateingFlag.SetPropertyValue(false);
            }
        }

        protected async Task<T> RunCommandAsync<T>(Expression<Func<bool>> updatingFlag, Func<Task<T>> action, T defaultValue = default(T))
        {

            if (updatingFlag.GetPropertyValue())
                return defaultValue;

            updatingFlag.SetPropertyValue(true);

            try
            {
                return await action();
            }
            finally
            {
                updatingFlag.SetPropertyValue(false);
            }
        }
    }
}