﻿namespace Szakdolgozat.Core
{
    public class MessageBoxViewModel : BaseViewModel
    {
        public string Title { get; set; }

        public string Message { get; set; }

        public string OkText { get; set; }
    }
}
