﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;

namespace Szakdolgozat.Core
{
    public class LoginViewModel : BaseViewModel
    {
        public LoginViewModel()
        {
            LoginCommand = new RelayParameterizedCommand(async parameter => await LoginAsync(parameter));
            OfflineCommand = new RelayCommand(ContinueOffline);
            RegisterCommand = new RelayParameterizedCommand(parameter => Register());
        }

        /// <summary>
        ///     Logs in
        /// </summary>
        /// <param name="parameter">The secure string from password TextBox</param>
        /// <returns></returns>
        public async Task LoginAsync(object parameter)
        {
            if (Email != null)
            {
                try
                {
                    var test = new MailAddress(Email);
                }
                catch (FormatException)
                {
                    Response = "Invalid Email format.";
                    return;
                }
            }
            else
            {
                Response = "Email?";
                return;
            }

            if ((parameter as ISecurePasswordHandler)?.SecurePassword?.Length < 6)
            {
                Response = "Password is too short.";
                return;
            }

            Response = string.Empty;
            try
            {
                await RunCommandAsync(() => LoginRunning, async () =>
                {
                    var request = (HttpWebRequest)WebRequest.Create("http://127.0.0.1:5000/api/login");
                    request.ContentType = "application/json";
                    request.Method = "POST";

                    if (request.CookieContainer == null) request.CookieContainer = new CookieContainer(10);
                    request.CookieContainer.Add(request.RequestUri, new Cookie("id", "teszt"));

                    using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                    {
                        var json = JsonConvert.SerializeObject(new
                        {
                            email = $"{Email.ToLower()}",
                            password = $"{(parameter as ISecurePasswordHandler)?.SecurePassword.Unsecure()}"
                        });

                        streamWriter.Write(json);
                    }

                    var response = (HttpWebResponse)request.GetResponse();

                    if (response.Cookies["success"]?.Value == "true")
                    {
                        // Successful login
                        IoC.Settings.UserName = response.Cookies["user"]?.Value;
                        IoC.Settings.UserId = int.Parse(response.Cookies["id"]?.Value);
                        IoC.Settings.Token = response.Cookies["token"]?.Value;
                        IoC.Settings.Email = Email;

                        var tokenModel = new TokenModel(IoC.Settings.UserId, IoC.Settings.UserName,
                            response.Cookies["token"].Value,
                            DateTime.Parse(Uri.UnescapeDataString(response.Cookies["validity"].Value)));

                        // Save the token
                        tokenModel.WriteToFile();

                        // Login to the app
                        IoC.Application.SideMenuVisible ^= true;
                        IoC.Application.CurrentPage = ApplicationPage.Main;

                        await IoC.Settings.GetImagesAsync();
                        await IoC.Settings.GetSharedAsync();
                    }
                    else
                    {
                        // Failed login
                        Response = Uri.UnescapeDataString(response.Cookies["message"]?.Value);
                    }
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Response = "Server is not responding";
            }
        }

        private void Register()
        {
            IoC.Application.CurrentPage = ApplicationPage.Register;
        }

        public void ContinueOffline()
        {
            // Wait for content to load
            IoC.Settings.UserName = "Offline";
            IoC.Application.SideMenuVisible ^= true;
            IoC.Application.CurrentPage = ApplicationPage.Main;
        }

        #region Public properties

        public string Email { get; set; }

        public string Response { get; set; }

        public bool LoginRunning { get; set; }

        #endregion

        #region Commands

        public ICommand LoginCommand { get; set; }
        public ICommand OfflineCommand { get; set; }
        public ICommand RegisterCommand { get; set; }

        #endregion
    }
}