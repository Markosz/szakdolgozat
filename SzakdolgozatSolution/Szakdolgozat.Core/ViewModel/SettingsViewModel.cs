﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;

namespace Szakdolgozat.Core
{
    public class SettingsViewModel : BaseViewModel
    {
        #region Public properties
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public ImageListModel ImageList { get; set; } = ImageListModel.Instance;
        public SharedListModel SharedList { get; set; } = SharedListModel.Instance;
        public bool SaveRunning { get; set; }

        #endregion

        #region Commands
        public ICommand OpenCommand { get; set; }
        public ICommand CloseCommand { get; set; }
        public ICommand SaveCommand { get; set; }
        public ICommand LogoutCommand { get; set; }

        #endregion

        public SettingsViewModel()
        {
            SaveCommand = new RelayParameterizedCommand(async (parameter) => await GetSharedAsync());
            LogoutCommand = new RelayCommand(Logout);

            OpenCommand = new RelayCommand(Open);
            CloseCommand = new RelayCommand(Close);
        }

        private void Logout()
        {
            File.Delete("CurrentUser.json");
            IoC.Application.CurrentUser = "";
            IoC.Settings.Email = "";
            IoC.Settings.UserName = "";
            IoC.Settings.UserId = 0;

            IoC.Settings.ImageList.Items.Clear();
            IoC.Settings.SharedList.Items.Clear();

            IoC.Application.GoToPage(ApplicationPage.Login);
        }

        public async Task GetImagesAsync()
        {
            if (IoC.Settings.UserName == "Offline") return;

            var imageList = new List<ImageModel>();

            await RunCommandAsync(() => SaveRunning, async () =>
            {
                var request = (HttpWebRequest)WebRequest.Create("http://127.0.0.1:5000/api/image");
                //request.ContentType = "application/json";
                request.Method = "GET";

                if (request.CookieContainer == null) request.CookieContainer = new CookieContainer(10);
                request.CookieContainer.Add(request.RequestUri, new Cookie("id", IoC.Settings.UserId.ToString()));
                request.CookieContainer.Add(request.RequestUri, new Cookie("token", IoC.Settings.Token));

                var response = (HttpWebResponse)request.GetResponse();

                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    var jsonResp = sr.ReadToEnd();
                    imageList = JsonConvert.DeserializeObject<List<ImageModel>>(jsonResp);
                }

                foreach (var image in imageList)
                {
                    if (GetImageAsync(image.Image))
                    {
                        if (IoC.Settings.ImageList.Items.Any(s => s.Image.Contains(image.Image))) continue;

                        IoC.Settings.ImageList.Items.Add(new ImageListItemViewModel
                        {
                            Id = image.ImageID,
                            Caption = image.Title,
                            Description = image.Description,
                            Image = System.AppDomain.CurrentDomain.BaseDirectory + $"Images\\{image.Image}"
                        });
                    }
                }
            });
        }

        /// <summary>
        /// Get the list of images that are shared with the user
        /// </summary>
        /// <returns></returns>
        public async Task GetSharedAsync()
        {
            if (IoC.Settings.UserName == "Offline") return;

            var imageList = new List<ImageModel>();

            await RunCommandAsync(() => SaveRunning, async () =>
            {
                var request = (HttpWebRequest)WebRequest.Create("http://127.0.0.1:5000/api/share");
                request.Method = "GET";

                if (request.CookieContainer == null) request.CookieContainer = new CookieContainer(10);
                request.CookieContainer.Add(request.RequestUri, new Cookie("id", IoC.Settings.UserId.ToString()));
                request.CookieContainer.Add(request.RequestUri, new Cookie("token", IoC.Settings.Token));

                var response = (HttpWebResponse)request.GetResponse();

                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    var jsonResp = sr.ReadToEnd();
                    imageList = JsonConvert.DeserializeObject<List<ImageModel>>(jsonResp);
                }

                foreach (var image in imageList)
                {
                    if (GetImageAsync(image.Image))
                    {
                        if (IoC.Settings.SharedList.Items.Any(s => s.Image.Contains(image.Image))) continue;

                        IoC.Settings.SharedList.Items.Add(new ImageListItemViewModel
                        {
                            Caption = image.Title,
                            Description = image.Description,
                            Image = System.AppDomain.CurrentDomain.BaseDirectory + $"Images\\{image.Image}"
                        });
                    }
                }
            });
        }

        private bool GetImageAsync(string image)
        {
            Directory.CreateDirectory("Images");

            // If we already have the image, don't download it.
            if (File.Exists($"Images\\{image}"))
            {
                return true;
            }

            var request = (HttpWebRequest)WebRequest.Create($"http://127.0.0.1:5000/api/image/{image}");
            request.Method = "GET";

            if (request.CookieContainer == null) request.CookieContainer = new CookieContainer(10);
            request.CookieContainer.Add(request.RequestUri, new Cookie("id", IoC.Settings.UserId.ToString()));
            request.CookieContainer.Add(request.RequestUri, new Cookie("token", IoC.Settings.Token));
            try
            {
                var response = (HttpWebResponse)request.GetResponseAsync().Result;

                using (var sr = new StreamReader(response.GetResponseStream()))
                {

                    var imageBytes = Convert.FromBase64String(sr.ReadToEnd());
                    File.WriteAllBytes($"Images\\{image}", imageBytes);

                    return true;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

        public void Open()
        {
            IoC.Application.SettingsMenuVisible = true;
        }

        public void Close()
        {
            IoC.Application.SettingsMenuVisible = false;
        }
    }
}