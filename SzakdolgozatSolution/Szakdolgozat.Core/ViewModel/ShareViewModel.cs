﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows.Controls;
using System.Windows.Input;
using Newtonsoft.Json;

namespace Szakdolgozat.Core
{
    public class ShareViewModel : BaseViewModel
    {
        public int ImageId { get; set; }
        public string Image { get; set; }
        public List<string> Users { get; set; }

        private ImageListItemViewModel parent;

        public ICommand BackCommand { get; set; }
        public ICommand ShareCommand { get; set; }


        public ShareViewModel()
        {

            var vm = (ImageListItemViewModel)IoC.Application.CurrentPageViewModel;
            parent = vm;

            ImageId = vm.Id;
            Image = vm.Image;
            Users = GetUserList();

            BackCommand = new RelayCommand(Back);
            ShareCommand = new RelayParameterizedCommand(parameter => Share(parameter));

        }

        private void Share(object param)
        {
            if (string.IsNullOrWhiteSpace(Image) || string.IsNullOrWhiteSpace(((ComboBox)param).SelectedItem?.ToString())) return;

            var user = ((ComboBox)param).SelectedItem.ToString();
            var request = (HttpWebRequest)WebRequest.Create($"http://127.0.0.1:5000/api/share");
            request.ContentType = "application/json";
            request.Method = "POST";

            if (request.CookieContainer == null) request.CookieContainer = new CookieContainer(10);
            request.CookieContainer.Add(request.RequestUri, new Cookie("id", IoC.Settings.UserId.ToString()));
            request.CookieContainer.Add(request.RequestUri, new Cookie("token", IoC.Settings.Token));
            request.CookieContainer.Add(request.RequestUri, new Cookie("user", user));

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                var data = new ShareModel
                {
                    ImageID = ImageId,
                    ImageOwnerId = IoC.Settings.UserId,
                    ShareTo = user
                };

                var json = JsonConvert.SerializeObject(data);

                streamWriter.Write(json);
            }

            var response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                IoC.Application.GoToPage(ApplicationPage.Main, parent);
            }
        }

        private void Back()
        {
            IoC.Application.GoToPage(ApplicationPage.Main, parent);
        }

        private List<string> GetUserList()
        {
            var image = Image.Substring(Image.LastIndexOf('\\') + 1);
            var request = (HttpWebRequest)WebRequest.Create($"http://127.0.0.1:5000/api/auth");
            request.Method = "GET";

            if (request.CookieContainer == null) request.CookieContainer = new CookieContainer(10);
            request.CookieContainer.Add(request.RequestUri, new Cookie("id", IoC.Settings.UserId.ToString()));
            request.CookieContainer.Add(request.RequestUri, new Cookie("token", IoC.Settings.Token));

            var response = (HttpWebResponse)request.GetResponse();

            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                var jsonResp = sr.ReadToEnd();
                var users = JsonConvert.DeserializeObject<List<string>>(jsonResp);

                return users;
            }
        }
    }
}
