﻿namespace Szakdolgozat.Core
{
    /// <summary>
    /// Test data
    /// </summary>
    public class ImageListItemDesignModel : ImageListItemViewModel
    {
        public static ImageListItemDesignModel Instance => new ImageListItemDesignModel();

        public ImageListItemDesignModel()
        {
            Caption = "Caption";

            Description = "Description";

            Image = "G:\\Szakdolgozat\\Resources\\Image\\test.jpg";

        }
    }
}
