﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Szakdolgozat.Core
{
    /// <summary>
    /// Test data
    /// </summary>
    public class ImageListDesignModel : ImageListViewModel
    {
        public static ImageListDesignModel Instance => new ImageListDesignModel();

        public ImageListDesignModel()
        {
            Items = new ObservableCollection<ImageListItemViewModel>
            {
                new ImageListItemViewModel
                {
                    Caption = "Leon",
                    Description = "This is a test",
                    Image = "G:\\Szakdolgozat\\Resources\\Image\\test.jpg"
                },
                new ImageListItemViewModel
                {
                    Caption = "Szisza",
                    Description = "This is a testű",
                    Image = "G:\\Szakdolgozat\\Resources\\Image\\testest.jpg",
                    //IsSelected = true
                },
                new ImageListItemViewModel
                {
                    Caption = "Loin",
                    Description = "This is a test",
                    Image = "G:\\Szakdolgozat\\Resources\\Image\\test.jpg"
                },
                new ImageListItemViewModel
                {
                    Caption = "Loin",
                    Description = "This is a test",
                    Image = "G:\\Szakdolgozat\\Resources\\Image\\DSC_5657-1.jpg"
                },
                new ImageListItemViewModel
                {
                    Caption = "Tica",
                    Description = "This is a test",
                    Image = "G:\\Szakdolgozat\\Resources\\Image\\testss.jpg"
                },
                new ImageListItemViewModel
                {
                    Caption = "Loin",
                    Description = "This is a test",
                    Image = "G:\\Szakdolgozat\\Resources\\Image\\test.jpg"
                },
                new ImageListItemViewModel
                {
                    Caption = "Loin",
                    Description = "This is a test",
                    Image = "G:\\Szakdolgozat\\Resources\\Image\\test.jpg"
                },
                new ImageListItemViewModel
                {
                    Caption = "Loin",
                    Description = "This is a test",
                    Image = "G:\\Szakdolgozat\\Resources\\Image\\test.jpg"
                },
                new ImageListItemViewModel
                {
                    Caption = "Loin",
                    Description = "This is a test",
                    Image = "G:\\Szakdolgozat\\Resources\\Image\\test.jpg"
                },
            };
        }
    }
}