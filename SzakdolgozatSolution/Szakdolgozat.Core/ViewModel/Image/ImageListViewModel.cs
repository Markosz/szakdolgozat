﻿using System.Collections.ObjectModel;

namespace Szakdolgozat.Core
{
    /// <summary>
    /// View model for the image list in the sidebar
    /// </summary>
    public class ImageListViewModel : BaseViewModel
    {
        public ObservableCollection<ImageListItemViewModel> Items { get; set; }
    }
}
