﻿using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Input;

namespace Szakdolgozat.Core
{
    /// <summary>
    /// View model for each image list item in the sidebar
    /// </summary>
    public class ImageListItemViewModel : BaseViewModel
    {
        #region Properties
        public int Id { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public bool IsSelected { get; set; }
        #endregion

        #region Commands
        public ICommand SelectCommand { get; set; }
        public ICommand EditCommand { get; set; }
        public ICommand PopoutCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand ShareCommand { get; set; }
        #endregion

        public ImageListItemViewModel()
        {
            SelectCommand = new RelayCommand(SelectItem);
            PopoutCommand = new RelayCommand(Popout);
            EditCommand = new RelayCommand(Edit);
            DeleteCommand = new RelayCommand(Delete);
            ShareCommand = new RelayCommand(Share);
        }

        // Open the sharing page
        private void Share()
        {
            if (IoC.Application.CurrentSidebarContent == SidebarContent.Shared) return;

            IoC.Application.GoToPage(ApplicationPage.Share, this);
        }

        private void Delete()
        {
            if (IoC.Application.CurrentSidebarContent == SidebarContent.Shared) return;
            if (string.IsNullOrWhiteSpace(Image)) return;

            var image = Image.Substring(Image.LastIndexOf('\\') + 1);
            var request = (HttpWebRequest)WebRequest.Create($"http://127.0.0.1:5000/api/image/{image}");
            request.Method = "DELETE";

            if (request.CookieContainer == null) request.CookieContainer = new CookieContainer(10);
            request.CookieContainer.Add(request.RequestUri, new Cookie("id", IoC.Settings.UserId.ToString()));
            request.CookieContainer.Add(request.RequestUri, new Cookie("token", IoC.Settings.Token));

            var response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                File.Delete(Image);
                IoC.Settings.ImageList.Items.Remove(IoC.Settings.ImageList.Items.FirstOrDefault(s => s.IsSelected));

                IoC.Application.GoToPage(ApplicationPage.Main, new ImageListItemViewModel());
            }
        }

        private void Edit()
        {
            if (IoC.Application.CurrentSidebarContent == SidebarContent.Shared) return;

            IoC.Application.GoToPage(ApplicationPage.Edit, this);
        }

        private void Popout()
        {
            IoC.Application.OpenPopout(ApplicationPage.Popout, this);
        }

        private void SelectItem()
        {
            // reset is selected
            foreach (var item in IoC.Settings.ImageList.Items)
            {
                item.IsSelected = false;
            }

            foreach (var item in IoC.Settings.SharedList.Items)
            {
                item.IsSelected = false;
            }

            IoC.Application.GoToPage(ApplicationPage.Main, new ImageListItemViewModel
            {
                Id = Id,
                Caption = Caption,
                Description = Description,
                Image = Image,
                IsSelected = true
            });

            IsSelected = true;
        }
    }
}