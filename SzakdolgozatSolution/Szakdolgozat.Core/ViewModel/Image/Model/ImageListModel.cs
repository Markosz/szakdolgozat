﻿using System;
using System.Collections.ObjectModel;

namespace Szakdolgozat.Core
{
    public class ImageListModel : ImageListViewModel
    {
        public static ImageListModel Instance => new ImageListModel();
        private Guid _id = Guid.NewGuid();

        public ImageListModel()
        {
            Items = new ObservableCollection<ImageListItemViewModel>();
        }
    }
}