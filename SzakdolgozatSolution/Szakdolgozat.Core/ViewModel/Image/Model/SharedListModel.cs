﻿using System;
using System.Collections.ObjectModel;

namespace Szakdolgozat.Core
{
    public class SharedListModel : ImageListViewModel
    {
        public static SharedListModel Instance => new SharedListModel();

        private Guid _id = Guid.NewGuid();

        public SharedListModel()
        {
            Items = new ObservableCollection<ImageListItemViewModel>();
        }
    }
}