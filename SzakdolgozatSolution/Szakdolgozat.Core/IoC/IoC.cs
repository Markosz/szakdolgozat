﻿using Ninject;

namespace Szakdolgozat.Core
{
    // Inversion of Control
    public static class IoC
    {
        public static IKernel Kernel { get; private set; } = new StandardKernel();

        public static IUIManager UI => IoC.Get<IUIManager>();

        public static ApplicationViewModel Application => Get<ApplicationViewModel>();

        public static SettingsViewModel Settings => Get<SettingsViewModel>();

        public static T Get<T>()
        {
            return Kernel.Get<T>();
        }

        public static void Setup()
        {
            // Bind view models
            BindViewModels();
        }

        /// <summary>
        /// Binds all view models
        /// </summary>
        private static void BindViewModels()
        {
            // Binds to a single instance of ApplicationViewModel, we access everything from here
            Kernel.Bind<ApplicationViewModel>().ToConstant(new ApplicationViewModel());
            Kernel.Bind<SettingsViewModel>().ToConstant(new SettingsViewModel());

            // Binds a temporary instance that gets created with each request
            Kernel.Bind<UploadViewModel>().ToSelf().InTransientScope();
        }
    }
}