﻿using System.Threading.Tasks;

namespace Szakdolgozat.Core
{
    // UI manager that handles UI interaction
    public interface IUIManager
    {
        Task ShowMessage(MessageBoxViewModel viewmodel);
    }
}