﻿namespace Szakdolgozat.Server.Models
{
    public class ImageModel
    {
        public string Caption { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
    }
}