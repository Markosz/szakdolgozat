﻿using System;
using MySql.Data.MySqlClient;

namespace Szakdolgozat.Server
{
    public interface IConnection
    {
        MySqlConnection GetOpenConnection();
    }

    public class Connection : IConnection
    {
        public static string ConnectionString { get; set; }

        public MySqlConnection GetOpenConnection()
        {
            var connection = new MySqlConnection(ConnectionString);

            try
            {
                connection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return connection;
        }
    }
}