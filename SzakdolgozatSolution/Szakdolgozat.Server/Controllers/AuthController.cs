﻿using System;
using System.Data;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Szakdolgozat.Core;

namespace Szakdolgozat.Server
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IConnection _conn;
        private readonly IAuthenticate _auth;

        public AuthController(IConnection conn, IAuthenticate auth)
        {
            _conn = conn;
            _auth = auth;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var id = int.Parse(Request.Cookies["id"]);
            var token = Request.Cookies["token"];

            if (!_auth.IsAuthenticated(id, token)) return Unauthorized();

            using IDbConnection conn = _conn.GetOpenConnection();
            var users = conn.Query<string>($"SELECT username FROM user WHERE iduser != {id}");
            var json = JsonConvert.SerializeObject(users);
            return Ok(json);
        }

        [HttpPost]
        public IActionResult Post([FromBody] TokenModel value)
        {
            using (var conn = _conn.GetOpenConnection())
            {
                using var comm = new MySqlCommand("SELECT token_value FROM token WHERE user_id = @id", conn);
                comm.Parameters.AddWithValue("@id", value.UserId);

                var token = (string)comm.ExecuteScalar() ?? "";

                if (token == value.Token)
                {
                    // Extend validity for the next 30 minutes
                    comm.CommandText = $"UPDATE token SET validity = '{DateTime.Now.AddMinutes(30):yyyy-MM-dd HH:mm:ss}' WHERE token_value = '{token}'";
                    comm.ExecuteNonQuery();

                    Response.Cookies.Append("success", "true");
                }
                else
                {
                    Response.Cookies.Append("success", "false");
                }
            }

            return Ok();
        }
    }
}