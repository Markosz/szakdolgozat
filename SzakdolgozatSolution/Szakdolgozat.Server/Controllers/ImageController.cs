﻿using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Szakdolgozat.Core;

namespace Szakdolgozat.Server.Controllers
{
    [Route("api/[controller]/")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private readonly IConnection _conn;
        private readonly IAuthenticate _auth;

        public ImageController(IConnection conn, IAuthenticate auth)
        {
            _conn = conn;
            _auth = auth;
        }

        /// <summary>
        ///     Get the user's images
        ///     Cookie should contain ID and Token
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var id = int.Parse(Request.Cookies["id"]);
            var token = Request.Cookies["token"];

            if (!_auth.IsAuthenticated(id, token)) return Unauthorized();

            using (IDbConnection conn = _conn.GetOpenConnection())
            {
                var images = conn.Query<ImageModel>($"SELECT idimage as ImageID, title, description, image FROM image WHERE ownerid = {id}");
                var json = JsonConvert.SerializeObject(images);
                return Ok(json);
            }
        }

        [HttpGet("{image}")]
        public async Task<IActionResult> Get(string image)
        {
            var id = int.Parse(Request.Cookies["id"]);
            var token = Request.Cookies["token"];
            string base64Image = string.Empty;

            if (!_auth.IsAuthenticated(id, token)) return Unauthorized();

            try
            {
                base64Image = Convert.ToBase64String(System.IO.File.ReadAllBytes($"Users\\{id}\\Images\\{image}"));
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
            // Get the image from server files

            if (string.IsNullOrWhiteSpace(base64Image))
            {
                using (IDbConnection conn = _conn.GetOpenConnection())
                {
                    await conn.ExecuteAsync($"DELETE FROM image WHERE image = '{image}'");
                    return null;
                }
            }

            return Ok(base64Image);
        }

        /// <summary>
        ///     Add new image
        /// </summary>
        /// <param name="value"></param>
        [HttpPost]
        public IActionResult Post([FromBody] ImageModel value)
        {
            var id = int.Parse(Request.Cookies["id"]);
            var token = Request.Cookies["token"];
            if (!_auth.IsAuthenticated(id, token)) return Unauthorized();

            // Create directory tree if it does not exists
            Directory.CreateDirectory($"Users\\{value.ImageOwnerId}\\Images\\");

            var serverPath = value.ImageName;

            var image = Convert.FromBase64String(value.Image);

            System.IO.File.WriteAllBytes($"Users\\{value.ImageOwnerId}\\Images\\" + serverPath, image);

            using (var comm = new MySqlCommand(
                    $"INSERT INTO image( ownerid, title, description, image, image_size, upload_date ) VALUES ({value.ImageOwnerId}, '{value.Title}', '{value.Description}', '{serverPath}', {value.ImageSize}, '{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}')",
                    _conn.GetOpenConnection()))
            {
                comm.CommandText = comm.CommandText.Replace(@"\", @"\\");
                try
                {
                    comm.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                    return Ok("Already exists.");
                }
            }

            return Ok("Upload complete.");
        }

        /// <summary>
        /// Replace the image file on the server and update the upload date on database
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult Put([FromBody] ImageModel value)
        {
            var id = int.Parse(Request.Cookies["id"]);
            var token = Request.Cookies["token"];
            if (!_auth.IsAuthenticated(id, token)) return Unauthorized();

            try
            {
                var image = Convert.FromBase64String(value.Image);
                System.IO.File.Delete($"Users\\{value.ImageOwnerId}\\Images\\" + value.ImageName);
                System.IO.File.WriteAllBytes($"Users\\{value.ImageOwnerId}\\Images\\" + value.ImageName, image);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            try
            {
                using (var conn = _conn.GetOpenConnection())
                {
                    conn.Execute($"UPDATE image SET upload_date = '{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}', image_size = {value.ImageSize} WHERE ownerid = {value.ImageOwnerId} AND image LIKE '%{value.ImageName}%'");
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }

            return Ok();
        }

        /// <summary>
        ///     Delete image
        /// </summary>
        /// <param name="image"></param>
        [HttpDelete("{image}")]
        public IActionResult Delete(string image)
        {
            var id = int.Parse(Request.Cookies["id"]);
            var token = Request.Cookies["token"];
            if (!_auth.IsAuthenticated(id, token)) return Unauthorized();

            try
            {
                System.IO.File.Delete($"Users\\{id}\\Images\\" + image);

                if (!System.IO.File.Exists($"Users\\{id}\\Images\\" + image))
                {
                    using (IDbConnection conn = _conn.GetOpenConnection())
                    {
                        conn.Execute($"DELETE FROM image WHERE image = '{image}' AND ownerid = {id}");
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }

            return Ok();
        }
    }
}