﻿using System;
using System.Data;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Szakdolgozat.Core;

namespace Szakdolgozat.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShareController : ControllerBase
    {
        private readonly IConnection _conn;
        private readonly IAuthenticate _auth;

        public ShareController(IConnection conn, IAuthenticate auth)
        {
            _conn = conn;
            _auth = auth;
        }

        /// <summary>
        /// Get what is shared with current user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var id = int.Parse(Request.Cookies["id"]);
            var token = Request.Cookies["token"];

            if (!_auth.IsAuthenticated(id, token)) return Unauthorized();

            using (IDbConnection conn = _conn.GetOpenConnection())
            {
                var images = conn.Query<ImageModel>($"SELECT title, description, image FROM image INNER JOIN share ON idimage = image_id WHERE to_id = {id}");
                var json = JsonConvert.SerializeObject(images);
                return Ok(json);
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] ShareModel value)
        {
            var id = int.Parse(Request.Cookies["id"]);
            var token = Request.Cookies["token"];

            if (!_auth.IsAuthenticated(id, token)) return Unauthorized();

            using (IDbConnection conn = _conn.GetOpenConnection())
            {
                var uid = conn.ExecuteScalar<int>($"SELECT iduser FROM user WHERE username = '{value.ShareTo}'");

                conn.Execute($"INSERT INTO share (image_id, from_id, to_id, share_date) VALUES ({value.ImageID}, {value.ImageOwnerId}, {uid}, '{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}')");
            }

            return Ok("Share complete.");
        }
    }
}