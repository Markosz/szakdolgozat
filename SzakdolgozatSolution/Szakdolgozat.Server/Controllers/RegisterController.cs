﻿using System;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;

namespace Szakdolgozat.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : ControllerBase
    {
        private readonly IConnection _conn;

        public RegisterController(IConnection conn)
        {
            _conn = conn;
        }

        [HttpPost]
        public IActionResult Post([FromBody] LoginModel value)
        {
            var pwBytes = System.Text.Encoding.ASCII.GetBytes(value.Password);
            var saltBytes = System.Text.Encoding.ASCII.GetBytes(value.Email);

            // Password hash salted with user's email
            var hash = HashFunction.Hash(pwBytes, saltBytes);

            using (MySqlConnection conn = _conn.GetOpenConnection())
            {
                using (MySqlCommand comm = new("INSERT INTO user (username, email, password) VALUES (@username, @email, @password)", conn))
                {
                    comm.Parameters.AddWithValue("@username", value.Username);
                    comm.Parameters.AddWithValue("@email", value.Email);
                    comm.Parameters.AddWithValue("@password", hash);

                    try
                    {
                        var res = comm.ExecuteNonQuery();

                        if (res >= 1)
                        {
                            return Ok("Registration completed");
                        }
                        else
                        {
                            return Ok("Registration failed");
                        }
                    }
                    catch (Exception exep)
                    {
                        if (exep.Message.Contains("Duplicate entry"))
                            return Ok("This email is already registered");

                        throw;
                    }
                }
            }
        }
    }
}