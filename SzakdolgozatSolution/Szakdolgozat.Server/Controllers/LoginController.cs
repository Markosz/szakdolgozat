﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;

namespace Szakdolgozat.Server
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IConnection _conn;

        public LoginController(IConnection conn)
        {
            _conn = conn;
        }

        // POST api/login
        [HttpPost]
        public IActionResult Post([FromBody] LoginModel value)
        {
            var pwBytes = System.Text.Encoding.ASCII.GetBytes(value.Password);
            var saltBytes = System.Text.Encoding.ASCII.GetBytes(value.Email);
            var hash = HashFunction.Hash(pwBytes, saltBytes);

            using MySqlConnection conn = _conn.GetOpenConnection();
            using MySqlCommand comm = new("SELECT password, iduser, username FROM user WHERE email = @email;", conn);
            comm.Parameters.AddWithValue("@email", value.Email);

            try
            {
                var reader = comm.ExecuteReader();

                string usr;
                uint id;
                if (reader.Read())
                {
                    id = reader.GetUInt32(1);
                    usr = reader.GetString(2);
                }
                else
                {
                    Response.Cookies.Append("success", "false");
                    Response.Cookies.Append("message", "Email is not registered");

                    return Ok();
                }

                reader.Close();

                var passw = (byte[])comm.ExecuteScalar();

                if (passw.SequenceEqual(hash))
                {
                    // Generate unique token
                    comm.CommandText = "generate_token";
                    comm.CommandType = CommandType.StoredProcedure;
                    comm.Parameters.AddWithValue("userId", id);

                    var validity = DateTime.Now.AddMinutes(30);
                    var token = (string)comm.ExecuteScalar();

                    Response.Cookies.Append("user", usr);
                    Response.Cookies.Append("id", id.ToString());
                    Response.Cookies.Append("token", token);
                    Response.Cookies.Append("validity", validity.ToString());
                    Response.Cookies.Append("success", "true");

                    return Ok();
                }
                else
                {
                    Response.Cookies.Append("success", "false");
                    Response.Cookies.Append("message", "Invalid password.");

                    return Ok();
                }
            }
            catch (System.Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw;
            }
        }
    }

}