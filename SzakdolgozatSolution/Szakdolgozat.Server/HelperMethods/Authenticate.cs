﻿using System;
using MySql.Data.MySqlClient;

namespace Szakdolgozat.Server
{
    public interface IAuthenticate
    {
        bool IsAuthenticated(int id, string token);
    }

    public class Authenticate : IAuthenticate
    {
        private readonly IConnection _conn;
        public Authenticate(IConnection conn)
        {
            _conn = conn;
        }

        public bool IsAuthenticated(int id, string token)
        {
            using (var comm = new MySqlCommand($"SELECT user_id, token_value, validity FROM token WHERE user_id = '{id}' ", (MySqlConnection)_conn.GetOpenConnection()))
            {
                var reader = comm.ExecuteReader();
                if (reader.Read())
                {
                    var resId = reader.GetInt32(0);
                    var resToken = reader.GetString(1);
                    var resValidity = reader.GetDateTime(2);

                    if (token == resToken && resValidity >= DateTime.Now)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
