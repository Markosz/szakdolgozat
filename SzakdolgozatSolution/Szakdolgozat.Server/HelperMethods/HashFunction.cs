﻿using System.Linq;
using System.Security.Cryptography;

namespace Szakdolgozat.Server
{
    public static class HashFunction
    {
        public static byte[] Hash(byte[] pw, byte[] salt)
        {
            byte[] saltedValue = pw.Concat(salt).ToArray();

            return new SHA512Managed().ComputeHash(saltedValue);
        }
    }
}